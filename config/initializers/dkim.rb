# Configure dkim globally (see above)
Dkim::domain      = 'styletag.com'
Dkim::selector    = 'ses3'
Dkim::private_key = open('private.key').read


# This will sign all ActionMailer deliveries
ActionMailer::Base.register_interceptor('Dkim::Interceptor')
Dkim::signable_headers = Dkim::DefaultHeaders - %w{Message-ID Resent-Message-ID Date Return-Path Bounces-To}
